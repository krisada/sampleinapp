//
//  ProductCell.swift
//  sample
//
//  Created by Krisada Kaewjunchai on 3/3/2558 BE.
//  Copyright (c) 2558 Cycle. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDesc: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var buttonBuy: UIButton!
    
    @IBAction func buy(sender: AnyObject) {
        
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
